import {Component} from 'angular2/core'

@Component({
    selector: 'gp-app',
    template: '<h1>Allons-y!</h1>'
})

export class AppComponent { }